import {connect} from "react-redux";
import GifsContainer from '../../components/home/gifsContainer.component'

const mapStateToProps = (state) => {
    return {
      gifsData: state.home.gifsData,
      selectedValue: state.home.selectedValue,
      isLoading: state.home.isLoading,
    };
  };
  
  const mapDispatchToProps = (dispatch) => {
    return {
      
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(GifsContainer)