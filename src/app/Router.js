import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'

import Home from './components/home/home.component'
import NotFound from './components/notFound/notFound.component'

const Router = () => (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/page-not-found" component={NotFound} />
      <Redirect path="*" to="/page-not-found" />
    </Switch>
)

export default Router
