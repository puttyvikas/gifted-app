import React from 'react';

import Router from './Router'
import Header from './components/header/header.component'
import Footer from './components/footer/footer.component'

const App = () => (
  <div>
    <Header />
    <main>
      <Router />
    </main>
    <Footer />
  </div>
)

export default App
