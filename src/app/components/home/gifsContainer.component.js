import React from 'react'
import PropTypes from 'prop-types';
import { Col } from 'antd';

import PostCard from "../shared/PostCard.component";

class GifsContainer extends React.Component {  
  render() {    
    console.log('from state', this.props.gifsData.data)
    return (
      <div>
        <h1>Showing results for '{this.props.selectedValue}'</h1>
        <div className="post-block">
          {
            this.props.gifsData.data.map(post => {
              return (
                <Col md={6} xs={12} key={post.id}>
                  <div className="product">
                    <PostCard post={post} isLoading={this.props.isLoading} handleChange={this.countVote} />
                  </div>
                </Col>
              )
            })            
          }
        </div>
      </div>
    )
  }
}

GifsContainer.propTypes = {
  gifsData: PropTypes.shape({}).isRequired,
  isLoading: PropTypes.bool.isRequired,
}

GifsContainer.defaultProps = {
  gifsData: [],
}
  
  export default GifsContainer
