import React from 'react'

import { Helmet } from "react-helmet"

import SearchGif from './searchGif.component'
import { Row, Col } from 'antd';
import GifsContainer from '../../containers/home/gifsContainer.container'

const Home = props => (
  <div>
    <Helmet>
        <title>GIFted</title>
        <meta name="description" content="GIFted app" />
        <meta name="keywords" content="GIFted, react" />
    </Helmet>
    <div className="main-section">            
        <SearchGif />
        <Row gutter={15}>
          <Col md={24}>
            <GifsContainer />
          </Col>
        </Row>        
    </div>
  </div>
  )
 export default Home
