import React from 'react'
import {connect} from "react-redux";
import { Input } from 'antd';

import { getGifData } from "../../actions/home.actions";

  class SearchGif extends React.Component {
    componentWillMount () {
      this.props.getGifData('gifs');
    }
    render() {
      return (
      <div className="select-gif">
        Search for GIF's: &nbsp;&nbsp;
        <Input onChange={(e) => this.props.getGifData(e.target.value)} placeholder="Search for GIF's" />
      </div>
    )
    }
  }

const mapStateToProps = (state) => {
  return {
    selectedValue: state.home.selectedValue,
  };
};

const mapDispatchToProps = (dispatch) => {
    return {
      getGifData: (item) => {
          dispatch(getGifData(item));
      }
    };
};

SearchGif.propTypes = {
}

SearchGif.defaultProps = {  
  getGifData: () => {},
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchGif)