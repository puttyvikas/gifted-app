import React from 'react'
import { Link } from 'react-router-dom'
export default () => {
  const style = {
    'text-align': 'center',
    'min-height': '640px',
    'font-size': '18px',
    'margin-top': '30px'
  }
  return (
    <div style={style}>
      <p style={{margin: 0}}>Page Not Found</p>
      <p style={{'font-size': '14px'}}>Go to <Link to="/">Home page</Link></p>
    </div>
  )  
}

