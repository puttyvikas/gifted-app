import React from 'react'
import { Button, Icon } from 'antd'
import { Link } from 'react-router-dom'

class Header extends React.Component {
  render() {
    return (
      <header>
        <div className='header'>
          <div className='flex-item'>
            <Link to='/'>GIFted</Link>
          </div>
          <a href='https://bitbucket.org/puttyvikas/gifted-app/src/master/' target='_blank' rel="noopener noreferrer" ><Button className='flex-item'><Icon type="fork" />Fork me on Bitbucket</Button></a>
        </div>
      </header>
    )
  }
}

export default Header

