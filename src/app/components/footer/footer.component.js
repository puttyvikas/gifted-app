import React from 'react'

const Footer = () => (
  <footer>
    <div className='footer'>
      <div className='flex-item'>About</div>
      <div className='flex-item'>Contact us</div>
    </div>
  </footer>
)

export default Footer

