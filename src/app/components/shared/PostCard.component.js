import React from 'react'
import { Card } from 'antd'
import FadeIn from "react-lazyload-fadein";

import CardMeta from './CardMeta.component'
const PostCard = props => (
  <Card hoverable loading={props.isLoading}>
  {/* {console.log('from --->', props.post)} */}
    <div className="ant-card-cover">
      <a href={props.post.url} target="_blank" rel="noopener noreferrer">
        <FadeIn>
          {onload => (
            <img
              src={props.post.images.original.url}
              onLoad={onload}
              alt={props.post.title}
            />
          )}
      </FadeIn>
      </a>
    </div>
    <CardMeta
      title={props.post.title}
    />
  </Card>
  )
 export default PostCard
