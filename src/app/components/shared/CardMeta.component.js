import React from 'react'

const CardMeta = props => (
  <div className="card-meta">
    <div className="title-holder">
      <div className="title">{props.title}</div>
    </div>
    <div className="description-wrapper">
      <p className="description">
        {props.description}
      </p>  
    </div>
  </div>
);

export default CardMeta