import { getGifs } from '../api/api'

export const loadingGifs = (val) => {
  return {
      type: "TOGGLE_SPINNER",
      payload: val
  }
}

const setGifs = (newState, item) => {
  return {
      type: "SET_GIF_DATA",
      payload: { newState, item }
  };
}

export const getGifData = (item) => (dispatch, getState) => {
  dispatch(loadingGifs(true))
  getGifs(item).then(function(response) {      
    dispatch(loadingGifs(false))    
    dispatch(setGifs(response.data, item));
   }).catch(error => {
      alert("Network Error")
  });
}