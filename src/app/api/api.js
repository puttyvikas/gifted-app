

import axios from 'axios';

export const GifApi = (query) => {  
  const API_KEY = 'BeU5aE30U2eCzfOaZ2nttmK2Ligls6Kp'
  return axios.get(`http://api.giphy.com/v1/gifs/search?q=${query}&api_key=${API_KEY}&limit=50`);
}

export const getGifs = (query) => {
  return GifApi(query)
}