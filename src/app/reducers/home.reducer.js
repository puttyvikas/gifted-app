
const HomeReducer = (state = {
  isLoading: false,
  selectedValue: 'gifs',
  gifsData: {
    data: [],
  },
  ups: 0,
  downs: 0,
}, action) => {
  switch (action.type) {
      case "TOGGLE_SPINNER":
          state = {
              ...state,
              isLoading: action.payload
          };
          break;
      case "SET_GIF_DATA": {
          state = {
            ...state,
            gifsData: action.payload.newState,
            selectedValue: action.payload.item,
          }
          break;
        }
      default:
          return state
  }
  return state;
};

export default HomeReducer;