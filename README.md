## React GIFted app
  This app bootstaped with create app.
  Tech - React 16.5.2, redux 4, and ant design for react components.

### To run the development server
```
yarn install
yarn start
```
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Folder Structure

```
my-app/
  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
    manifest.json
  src/
    app/
      components/
      containers/
      actions/
      reducers/
      api/
      App.js
      App.test.js
    assets/
      fonts/
      images/
      styles/ 
    index.js
    store.js
    serviceWorker.js
```

For the project to build, **these files must exist with exact filenames**:

- `public/index.html` is the page template;
- `src/index.js` is the JavaScript entry point.


You may create subdirectories inside `src`. For faster rebuilds, only files inside `src` are processed by Webpack.<br>
You need to **put any JS and CSS files inside `src`**, otherwise Webpack won’t see them.

Only files inside `public` can be used from `public/index.html`.<br>

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

